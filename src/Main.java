import java.io.*;
import java.util.*;


public class Main {
    public static int   TariffPlanFileFirstRatingParameterColumn = 0, TariffPlanFileLastRatingParameterColumn = 1, TariffPlanFileTariffNameColumn = 3,
                        TariffsFileTariffNameColumn = 0, TariffsFileTariffUuidColumn = 1;

    public static String[] fileNamesCSV = {"Tariffs.csv", "TariffPlan.csv"};
    public static HashMap<String, String> tariffNameToUUIDs = new HashMap<>();
    public static List<String[]> layoutRows = new LinkedList<>();
    public static String planName = "SMS OffLine Roaming";

    public static String parameterUUID = "",
            ratingParameterSetUUID = "f49636ad-85a4-4f97-b848-3790c4ba6554",
            layoutUUID = "{{LayoutID}}",
            layoutDescription = null;

    public static String
            layoutStart = "{\n" +
            "    \"realm\": \"\",\n" +
            "    \"version\": 0,\n" +
            "    \"name\": \"",

    layoutMiddleBeforeDescription = "\",\n" +
            "    \"description\":",

    layoutMiddleAfterDescription = ",\n" +
            "    \"data\": [",

    layoutEnd = "],\n" +
            "    \"ratingParameterSetId\": \"" + ratingParameterSetUUID + "\",\n" +
            "    \"isIndividual\": false,\n" +
            "    \"isDeleted\": false\n" +
            "}",


    tariffPlanStart = "{\n" +
            "    \"calendarId\": null,\n" +
            "    \"description\": null,\n" +
            "    \"isDeleted\": false,\n" +
            "    \"isIndividual\": false,\n" +
            "    \"layoutId\": \"" + layoutUUID + "\",",

    tariffPlanEnd = ",\n" +
            "    \"realm\": \"\",\n" +
            "        \"version\": 0\n" +
            "}",


    tariffStart = "\n{\n" +
            "            \"tariffId\": \"",

    tariffMiddle1 = "\",\n" +
            "            \"list\": [\n" +
            "                {\n" +
            "                    \"calendarPropertiesId\": null,\n" +
            "                    \"id\": \"",

    tariffMiddle2 = "\",\n" +
            "                    \"position\": ",

    tariffEnd = "\n" +
            "                }\n" +
            "            ]\n" +
            "        },",



    groupInLayoutStart = "\"parameterGroupName\": null",

    groupInLayoutMiddle = ",\n" +
            "              \"value\": [\n",

    groupInLayoutEnd = "              \n]\n" +
            "            },\n" +
            "            {\n" +
            "              \"parameterGroupName\": null,\n" +
            "              \"value\": [",


    startGroup = "{\n" +
            "        \"realm\": \"\",\n" +
            "        \"name\": \"",

    middleGroup = "\",\n" +
            "        \"description\": \"\",\n" +
            "        \"values\": [\n",

    endGroup = "\n" +
            "        ],\n" +
            "        \"parameters\": [" + parameterUUID + "],\n" +
            "        \"isDeleted\": false\n" +
            "    }",


    layoutRowStart = "\n" +
            "        {\n" +
            "            \"position\":",

    layoutRowMiddle1 = ",\n" +
            "            \"parameters\": [\n" +
            "                {",

    layoutRowMiddle2 = "\n" +
            "                    ]\n" +
            "                }\n" +
            "            ],\n" +
            "            \"tags\": [",

    layoutRowEnd = "\n" +
            "            ],\n" +
            "            \"group\": null,\n" +
            "            \"isDeleted\": false\n" +
            "        },";


    public static void main(String[] args) {
        Date start = new Date();
        System.out.println(start);
        System.out.println();
        System.out.println();

        try {
            getTariffUUIDs();
            getLauyoutRows();
            PrintLayoutsAndTariffPlans();
        } catch (IOException ignore) {

        }


        System.out.println();
        System.out.println();
        Date end = new Date();
        System.out.println(end);
        System.out.println("Running took " + (end.getTime() - start.getTime()) / 1000.0 + " Seconds");
    }

    private static void getLauyoutRows() throws IOException {
        String row;
        BufferedReader csvReader = new BufferedReader(new FileReader(fileNamesCSV[1]));
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
            for (int i = 0; i < data.length; i++) {
                String data1 = data[i];
                data1 = data1.trim().replaceAll("\"", "");
                data[i] = data1;
            }
            layoutRows.add(data);

        }
    }

    private static void getTariffUUIDs() throws IOException {
        String row;
        BufferedReader csvReader = new BufferedReader(new FileReader(fileNamesCSV[0]));
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
            for (int i = 0; i < data.length; i++) {
                String data1 = data[i];
                data1 = data1.trim().replaceAll("\"", "");
                data[i] = data1;
            }
            String value = data[TariffsFileTariffUuidColumn],
                    key = data[TariffsFileTariffNameColumn];
            tariffNameToUUIDs.put(key, value);

        }
    }

    private static void PrintLayoutsAndTariffPlans() throws FileNotFoundException {

        StringBuilder layout = new StringBuilder();
        StringBuilder tariffs = new StringBuilder(tariffPlanStart + "\"name\": \"" + planName + " TariffPlan\",\n" +
                "    \"ranges\": [");
        layoutDescription = null;
        layout.append(layoutStart);
        int position = 0;
        Map<String, String> duplications = new HashMap<>();
        PrintWriter pwLayout= new PrintWriter(new File(planName + "_Layout.txt"));
        PrintWriter pwTariffs = new PrintWriter(new File(planName + "_Tariffs.txt"));

        layout.append(planName).append(" Layout").append(layoutMiddleBeforeDescription).append(layoutDescription).append(layoutMiddleAfterDescription);

        try {
            int size = layoutRows.size();
            for (int j = 0; j < size; j++, position++) {
                String[] data = layoutRows.get(j);
                String location = data[TariffPlanFileFirstRatingParameterColumn], operator = data[TariffPlanFileLastRatingParameterColumn], tariffName = data[TariffPlanFileTariffNameColumn];
                if (duplications.get(operator + location) == null) {
                    duplications.put(operator + location, "null");
                    String tariffUUID = tariffNameToUUIDs.get(tariffName);
                    layout.append(layoutRowStart)
                            .append(position)
                            .append(layoutRowMiddle1)
                            .append(groupInLayoutStart)
                            .append(groupInLayoutMiddle)
                            .append("\"").append(location).append("\"")
                            .append(groupInLayoutEnd)
                            .append("\"").append(operator).append("\"")
                            .append(layoutRowMiddle2)
                            .append(layoutRowEnd);

                    tariffs.append(tariffStart)
                            .append(tariffUUID)
                            .append(tariffMiddle1)
                            .append(tariffUUID)
                            .append(tariffMiddle2)
                            .append(position)
                            .append(tariffEnd);
                } else {
                    position--;
                }
//                        System.out.println(position);
            }

        } catch (NullPointerException e) {
            System.out.println("Null Pointer");
        }


        layout = new StringBuilder(layout.substring(0, layout.length() - 1) + layoutEnd);
        tariffs = new StringBuilder(tariffs.substring(0, tariffs.length() - 1) + "\n]" + tariffPlanEnd);
        pwLayout.println(layout);
        pwTariffs.println(tariffs);
        pwLayout.close();
        pwTariffs.close();

    }



}






































