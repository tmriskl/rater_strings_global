import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;

import java.io.*;
import java.util.*;


public class MainOld {
    public static String[]  fileNamesXLS = {""/*""TP21.xls"*/, "TariffsIDs1.xls", "COS_TP.xls"};
    public static String[]  fileNamesCSV = {""/*"TP.csv"*/, "TariffPlans.csv"};
    public static HashMap<String,List<String>> CosIdToGroups = new HashMap<>();
    public static Map<String,String> GroupToTariffPlan = new HashMap<>();
    public static Map<String,Map<String,List<String[]>>> CosIdToTariffPlanToDestinations = new HashMap<>();
    public static HashMap<String,String> tariffNameToUUIDs = new HashMap<>();

    public static HashMap<String,String> groups = new HashMap<>();
    public static String    parameterUUID = "",
            rpSet = "02b7fac5-eb15-4f9f-a6cc-72613b8f7e07",
            layoutDescription = null;

    public static String
            layoutStart =                       "{\n" +
            "    \"realm\": \"\",\n" +
            "    \"version\": 0,\n" +
            "    \"name\": \"",

    layoutMiddleBeforeDescription =     "\",\n" +
            "    \"description\":",

    layoutMiddleAfterDescription =      ",\n" +
            "    \"data\": [",

    layoutEnd =                         "],\n" +
            "    \"ratingParameterSetId\": \""+rpSet+"\",\n" +
            "    \"isIndividual\": false,\n" +
            "    \"isDeleted\": false\n" +
            "}",


    tariffStart =                       "\n{\n" +
            "            \"tariffId\": \"",

    tariffMiddle1 =                     "\",\n" +
            "            \"list\": [\n" +
            "                {\n" +
            "                    \"calendarPropertiesId\": null,\n" +
            "                    \"id\": \"",

    tariffMiddle2 =                     "\",\n" +
            "                    \"position\": ",

    tariffEnd =                         "\n" +
            "                }\n" +
            "            ]\n" +
            "        },",


    groupInLayoutStart =                "\"parameterGroupName\":\"",

    groupInLayoutMiddle =               "\",\n" +
            "              \"value\": [\n",

    groupInLayoutEnd =                  "              \n]\n" +
            "            },\n" +
            "            {\n" +
            "              \"parameterGroupName\": null,\n" +
            "              \"value\": [",


    startGroup =                        "{\n" +
            "        \"realm\": \"\",\n" +
            "        \"name\": \"",

    middleGroup =                       "\",\n" +
            "        \"description\": \"\",\n" +
            "        \"values\": [\n",

    endGroup =                          "\n" +
            "        ],\n" +
            "        \"parameters\": ["+parameterUUID+"],\n" +
            "        \"isDeleted\": false\n" +
            "    }",


    layoutRowStart =                    "\n" +
            "        {\n" +
            "            \"position\":",

    layoutRowMiddle1 =                  ",\n" +
            "            \"parameters\": [\n" +
            "                {",

    layoutRowMiddle2 =                  "\n" +
            "                    ]\n" +
            "                }\n" +
            "            ],\n" +
            "            \"tags\": [",

    layoutRowEnd =                      "\n" +
            "            ],\n" +
            "            \"group\": null,\n" +
            "            \"isDeleted\": false\n" +
            "        },";




    public static void main(String[] args) {
        Date start = new Date();
        System.out.println(start);
        System.out.println();
        System.out.println();

        try {
            getDestinations();
            getTariffUUIDs();
            PrintParametersGroups();
            PrintLayoutsAndTariffPlans();
        } catch (IOException ignore) {

        }
//        boolean[] methods = {true , true , false, false, false, true };
//        int i = 0;
//        if(methods[i]){
//            try {
//                getDestinations();
//                getTariffUUIDs();
//            } catch (IOException ignore) {
//
//            }
//        }
//        i++;
//
//        if (methods[i]){
//            try {
//                PrintParametersGroups();
//            } catch (IOException ignore) {
//
//            }
//        }
//        i++;
//
//        if (methods[i]){
//            try {
//                PrintLayoutRows();
//            } catch (IOException ignore) {
//
//            }
//        }
//        i++;
//
//
//        if (methods[i]){
//            try {
//                PrintFixedTariffName();
//            } catch (IOException ignore) {
//
//            }
//        }
//        i++;
//
//
//        if (methods[i]){
//            try {
//                PrintTariffsInLayoutOrder();
//            } catch (IOException ignore) {
//
//            }
//        }
//
//        i++;
//
//
//        if (methods[i]) {
//            try {
//                PrintLayoutsAndTariffPlans();
//            } catch (IOException ignore) {
//
//            }
//        }

        System.out.println();
        System.out.println();
        Date end = new Date();
        System.out.println(end);
        System.out.println("Running took " + (end.getTime() - start.getTime())/1000.0/60 + " Minutes");
    }

    private static void PrintLayoutsAndTariffPlans() throws FileNotFoundException {
        StringBuilder layouts = new StringBuilder("[");

/////////////////////COS_LEVEL/////////////////////////////////
        for(Map.Entry<String, List<String>> entry :
                CosIdToGroups.entrySet()){
            Date startCos = new Date();
            String cosID = entry.getKey();
            List<String> groupsNames = entry.getValue();
            String CosName = groupsNames.get(0).split("\\|")[0];
            StringBuilder layout = new StringBuilder();
            StringBuilder tariffs = new StringBuilder("\"name\": \"" + CosName + " TariffPlan\",\n" +
                    "    \"ranges\": [");
            PrintWriter pwLayout = new PrintWriter(new File("COSs\\" + CosName + "_Layout.txt"));
            PrintWriter pwTariffs = new PrintWriter(new File("COSs\\" + CosName + "_Tariffs.txt"));
            layoutDescription = null;

            System.out.println("\n" + CosName + "-" + cosID);

            layout.append(layoutStart).append(CosName).append(" Layout").append(layoutMiddleBeforeDescription).append(layoutDescription).append(layoutMiddleAfterDescription);


//////////////////////////////////////////////////////GROUP_LEVEL//////////////////////////////////
            int position = 0;
            for(int i = 0; i < groupsNames.size();i++) {
                String groupName = groupsNames.get(i);
                String tariffPlan = GroupToTariffPlan.get(groupName);
                List<String[]> destinationsData = CosIdToTariffPlanToDestinations.get(cosID).get(tariffPlan);
                if(destinationsData == null){
                    System.out.println();
                }


//////////////////////////////////////////////DESTINATION_LEVEL//////////////////////////////////
                for (int j = 0; j < Objects.requireNonNull(destinationsData).size(); j++, position++) {
                    String[] data = destinationsData.get(j);
                    String destination = data[2], destName = data[3], tariffName = data[4];
                    String tariffUUID = tariffNameToUUIDs.get(tariffName),
                            group = groups.get(groupName);

                    layout.append(layoutRowStart).append(position).append(layoutRowMiddle1).append(group).append("\"").append(destination).append("\"").append(layoutRowMiddle2).append("\"").append(destName).append("\",\"").append(tariffPlan).append("\"").append(layoutRowEnd);
                    tariffs.append(tariffStart).append(tariffUUID).append(tariffMiddle1).append(tariffUUID).append(tariffMiddle2).append(position).append(tariffEnd);

//                        System.out.println(position);
                }

            }

            layout = new StringBuilder(layout.substring(0, layout.length() - 1) + layoutEnd);
            tariffs = new StringBuilder(tariffs.substring(0, tariffs.length() - 1) + "\n]");
            pwLayout.println(layout);
            pwTariffs.println(tariffs);
            pwLayout.close();
            pwTariffs.close();
            layouts.append("\n").append(layout).append(",");

            System.out.println(position + " rows" + "\nTook " + (new Date().getTime() - startCos.getTime())/1000.0 + " Seconds" + "\n");

        }
        System.out.println(CosIdToGroups.size() + " " + CosIdToTariffPlanToDestinations.size() + " COSs");

        PrintWriter pw = new PrintWriter(new File("COSs\\" + "0_All_Layouts.txt"));
        layouts = new StringBuilder(layouts.substring(0, layouts.length() - 1) + "\n]");
        pw.println(layouts);
        pw.close();
    }

    private static void getDestinations() throws IOException {
        String row;
        BufferedReader csvReader = new BufferedReader(new FileReader(fileNamesCSV[1]));
        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
            for(int i = 0; i < data.length; i++){
                String data1 = data[i];
                data1 = data1.trim().replaceAll("\"","");
                data[i] = data1;
            }

            CosIdToTariffPlanToDestinations.computeIfAbsent(data[0], k -> new HashMap<>());
            CosIdToTariffPlanToDestinations.get(data[0]).computeIfAbsent(data[1], k -> new LinkedList<>());
            CosIdToTariffPlanToDestinations.get(data[0]).get(data[1]).add(data);

        }
        csvReader.close();
    }

    private static void PrintParametersGroups() throws IOException {
        FileInputStream fis = new FileInputStream(new File(fileNamesXLS[2]));
        HSSFWorkbook workbook = new HSSFWorkbook(fis);
        List<HSSFSheet> sheets = new LinkedList<>();
        for(int i = 0;i<workbook.getNumberOfSheets();i++)
            sheets.add(workbook.getSheetAt(i));

        HSSFSheet sheetGroups = sheets.get(0);

        HashMap<String,List<String>> codes = new HashMap<>();

        for(int i = 1 ; i< sheetGroups.getPhysicalNumberOfRows(); i++) {
            Row row = sheetGroups.getRow(i);
            try {
                String  COS_Name = row.getCell(0).getStringCellValue().trim(),
                        COS_ID = row.getCell(1).getStringCellValue().trim(),
                        ZoneSet_Name = row.getCell(3).getStringCellValue().trim(),
                        Zone_Name = row.getCell(5).getStringCellValue().trim(),
                        TP_Name = row.getCell(6).getStringCellValue().trim(),
                        Access_Code = row.getCell(9).getStringCellValue().trim();
                String  key = COS_Name + "|" + ZoneSet_Name + "|" + Zone_Name,
                        value = Access_Code.trim();

                CosIdToGroups.computeIfAbsent(COS_ID, k -> new LinkedList<>());
                CosIdToGroups.get(COS_ID).add(key);

                GroupToTariffPlan.computeIfAbsent(key, k -> TP_Name.replaceAll("\"",""));
                codes.computeIfAbsent(key, k -> new LinkedList<>());

                codes.get(key).add(value);
            }catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        StringBuilder output = new StringBuilder("[");



        for (Map.Entry<String,List<String>> entry:
                codes.entrySet()) {
            List<String> list = entry.getValue();
            String key = entry.getKey();
            groups.put(key, groupInLayoutStart + key + groupInLayoutMiddle);

            StringBuilder outputGroup = new StringBuilder();
            for(int i = 0; i<list.size();i++) {
                String Access_Code = list.get(i);
                outputGroup.append("\n\"").append(Access_Code).append("\",");
            }
            outputGroup = new StringBuilder(outputGroup.substring(0, outputGroup.length() - 1));
            groups.put(key, groups.get(key) + outputGroup + groupInLayoutEnd);
            output.append(startGroup).append(key).append(middleGroup).append(outputGroup).append(endGroup).append(",");
        }


        output = new StringBuilder(output.substring(0, output.length() - 1) + "]");
//        System.out.println(output);
        PrintWriter pw = new PrintWriter(new File("OldParameterGroups.txt"));
        pw.println(output);
        pw.close();

    }

//    private static void PrintTariffsInLayoutOrder() throws IOException {
//        FileInputStream fis = new FileInputStream(new File(fileNamesXLS[0]));
//        HSSFWorkbook workbook = new HSSFWorkbook(fis);
//        List<HSSFSheet> sheets = new LinkedList<>();
//        for(int i = 0;i<workbook.getNumberOfSheets();i++)
//            sheets.add(workbook.getSheetAt(i));
//
//        HSSFSheet sheetLayout = sheets.get(0);
//
//
//
//        String output = "[";
//        for(int i = 1 ; i< sheetLayout.getPhysicalNumberOfRows(); i++) {
//            Row row = sheetLayout.getRow(i);
//            try {
//                String TariffName = row.getCell(3).getStringCellValue().trim();
////                if(i>310){
////                    System.out.println();
////                    String t = UUIDs.get(TariffName);
////                    t = UUIDs.get(TariffName);
////                }
//                output += tariffStart +
//                        tariffNameToUUIDs.get(TariffName) +
//                        tariffMiddle1 +
//                        tariffNameToUUIDs.get(TariffName) +
//                        tariffMiddle2 +
//                        (i-1) +
//                        tariffEnd;
//
//            }catch (NullPointerException e) {
//                e.printStackTrace();
//            }
//        }
//        output = output.substring(0,output.length()-1)+"]";
//        System.out.println(output);
//        PrintWriter pw = new PrintWriter(new File("PrintTariffsInPostPaidOrder.txt"));
//        pw.println(output);
//        pw.close();
//
//    }

    private static void getTariffUUIDs() throws IOException {
        FileInputStream fis = new FileInputStream(new File(fileNamesXLS[1]));
        HSSFWorkbook workbook = new HSSFWorkbook(fis);
        LinkedList<HSSFSheet> sheets = new LinkedList<>();
        for(int i = 0;i<workbook.getNumberOfSheets();i++)
            sheets.add(workbook.getSheetAt(i));
        HSSFSheet sheetTariffs = sheets.get(0);

        for(int i = 0 ; i< sheetTariffs.getPhysicalNumberOfRows(); i++) {
            Row row = sheetTariffs.getRow(i);
            String value = row.getCell(0).getStringCellValue().trim(),
                    key = row.getCell(1).getStringCellValue().trim();
            tariffNameToUUIDs.put(key,value);
        }
    }

//    private static void PrintFixedTariffName() throws IOException {
//        FileInputStream fis = new FileInputStream(new File(fileNamesXLS[1]));
//        HSSFWorkbook workbook = new HSSFWorkbook(fis);
//        List<HSSFSheet> sheets = new LinkedList<>();
//        for(int i = 0;i<workbook.getNumberOfSheets();i++)
//            sheets.add(workbook.getSheetAt(i));
//        HSSFSheet sheetTariffs = sheets.get(0);
//
//        String output = "";
//        for(int i = 0 ; i< sheetTariffs.getPhysicalNumberOfRows(); i++) {
//            Row row = sheetTariffs.getRow(i);
//            String tariffName = row.getCell(1).getStringCellValue().trim();
//            tariffName = tariffName.replaceAll("\"","").replaceAll(",","");
//            System.out.println(tariffName);
//            output += tariffName + "\n";
//        }
//        workbook.close();
//        fis.close();
//
//        PrintWriter pw = new PrintWriter(new File("OldParameterGroups.txt"));
//        pw.println(output);
//        pw.close();
//    }



//    public static void PrintLayoutRows() throws IOException {
//        FileInputStream fis = new FileInputStream(new File(fileNamesXLS[0]));
//        HSSFWorkbook workbook = new HSSFWorkbook(fis);
//        List<HSSFSheet> sheets = new LinkedList<>();
//        for(int i = 0;i<workbook.getNumberOfSheets();i++)
//            sheets.add(workbook.getSheetAt(i));
//
//        HSSFSheet sheet = sheets.get(0);
//        HashMap<String,String> outputs = new HashMap<>();
//
//        String output = "[";
//
//
//
//
//
//        for(int i = 1 ; i< sheet.getPhysicalNumberOfRows(); i++) {
//            Row row = sheet.getRow(i);
//            try {
//                String tariffPlanName = row.getCell(0).getStringCellValue().trim(),
//                        destName = row.getCell(1).getStringCellValue().trim(),
//                        destination = row.getCell(2).getStringCellValue().trim(),
//
//                        group = row.getCell(4).getStringCellValue().trim();
//
//
//                output += layoutRowStart + (i - 1) + layoutRowMiddle1 + group + "\"" + destination + "\"" + layoutRowMiddle2 + "\"" + destName + "\",\"" + tariffPlanName + "\"" + layoutRowEnd;
//
//            }catch (NullPointerException e) {
//                e.printStackTrace();
//            }
//        }
//
//        output = output.substring(0,output.length()-1)+"]";
//        System.out.println(output);
//
//        PrintWriter pw = new PrintWriter(new File("PrintLayoutRows.txt"));
//        pw.println(output);
//        pw.close();
//    }
}





































